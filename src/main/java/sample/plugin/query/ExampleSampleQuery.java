package sample.plugin.query;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.osgi.extensions.annotation.ServiceReference;
import org.squashtest.tm.api.report.criteria.Criteria;
import org.squashtest.tm.api.report.query.ReportQuery;
import org.squashtest.tm.api.repository.SqlQueryRunner;

public class ExampleSampleQuery implements ReportQuery{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ExampleSampleQuery.class);
	
	
	private static final String TEST_CASE_NAME_LIKE = "name";
	


	/**
	 * The following resources will be injected via Spring, and corresponds to your queries. 
	 * Please refer to META-INF/spring/bundle-context.xml and check the definition of the bean
	 * id="query". 
	 * 
	 *  You can also write your SQL as a static string, your call.
	 */
	private Resource unfilteredQuery;
	private Resource nameFilteredQuery;
	
	

	public void setUnfilteredQuery(Resource unfilteredQuery) {
		this.unfilteredQuery = unfilteredQuery;
	}

	public void setNameFilteredQuery(Resource nameFilteredQuery) {
		this.nameFilteredQuery = nameFilteredQuery;
	}
	

	
	/**
	 * The OSGI platform gratefully gives you this service that will run your SQL queries. 
	 * Notice the @ServiceReference on the <strong>setter</strong>, that let Spring DM do the service injection for you.
	 * 
	 * @param runner
	 */
	private SqlQueryRunner runner;
	

    @ServiceReference
    public void setRunner(SqlQueryRunner runner) {
            this.runner = runner;
    }
	

	@Override
	public void executeQuery(Map<String, Criteria> criteria, Map<String, Object> model) {
		
		List<Object[]> rawData = getData(criteria);
		
		ExampleDataProcessor processor = new ExampleDataProcessor(rawData);
		processor.process();
		
		model.put("data", processor.getResult());
				
	}
	
	/**
	 * <p>That method will run the query using the arguments provided by the user.</p> 
	 * 
	 * <p>We expect a single argument here : the string that the tests cases we look for should contain. 
	 * If it is null or void we will use the unfiltered  query, otherwise we use the name-filtered query 
	 * using its value as an argument.</p>
	 * 
	 * <p>In this case we know that the said argument will be a string, hence the brutal treatment below.</p> 
	 * 
	 */
	private List<Object[]> getData(Map<String, Criteria> criteria){
		
		if (hasCriteria(criteria)){

			String query = readQuery(nameFilteredQuery);
			
			String nameCrit = "%"+criteria.get(TEST_CASE_NAME_LIKE).getValue().toString().trim()+"%";			
			Map<String, String> params = new HashMap<String, String>(1);
			params.put("name", nameCrit);
			
			return runner.executeSelect(query, params);
			
		}
		else{
			
			String query = readQuery(unfilteredQuery);
			
			return runner.executeSelect(query);
			
		}
		
	}
	
	
	private boolean hasCriteria(Map<String, Criteria> criteria){
		return (
				criteria.containsKey(TEST_CASE_NAME_LIKE) &&
				! (criteria.get(TEST_CASE_NAME_LIKE).getValue().toString().trim().isEmpty())
		);
	}
	
	
	private String readQuery(Resource resource){
		
		try{
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
			StringBuilder queryBuilder = new StringBuilder();
			String buffer;
			
			while((buffer=reader.readLine())!=null){
				queryBuilder.append(buffer+"\n");
			}
			
			return queryBuilder.toString();
			
		}catch(IOException ex){
			LOGGER.error("could not process query due to I/O error while reading the sql file : ", ex);
			throw new RuntimeException(ex);
		}
			
		
		
	}
	

	
}
