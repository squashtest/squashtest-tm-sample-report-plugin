package sample.plugin.query;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import sample.plugin.beans.ExampleProjectBean;
import sample.plugin.beans.ExampleTestCaseBean;

/**
 * That class will chew data incoming from the database and spit formatted data instead. Note that : 
 * 
 * <ol>
 * 	<li>it consumes the input and removes each record as soon as it has been processed : the input cannot be reused afterward.</li>
 * 	<li>it assumes that the query returns sorted results (which is fortunately the case here)</li>
 * </ol>
 * 
 * @author bsiri
 *
 */
public class ExampleDataProcessor {
	
	private List<Object[]> rawData;
	
	private List<ExampleProjectBean> result = new LinkedList<ExampleProjectBean>();
	
	private Long _currentId = null;
	private ExampleProjectBean _currentProject = null;
	
	
	public ExampleDataProcessor(List<Object[]> data){
		rawData = data;
	}
	
	
	
	public void process(){
		
		Iterator<Object[]> iterator = rawData.iterator();
		
		while (iterator.hasNext()){
			
			Object[] newRecord = iterator.next();
			
			if (hasProjectChanged(newRecord)){
				changeProject(newRecord);
			}
			
			addTestCase(newRecord);
			
			iterator.remove();
		}
		
	}
	
	
	public List<ExampleProjectBean> getResult(){
		return result;
	}
	
	
	
	private boolean hasProjectChanged(Object[] record){
		Long projectId = castToLong(record[0]);
		return (! projectId.equals(_currentId));
	}
	
	
	private void changeProject(Object[] record){
		_currentId = castToLong(record[0]);
		_currentProject = new ExampleProjectBean();
		_currentProject.setName(record[1].toString());
		
		result.add(_currentProject);
	}
	
	private void addTestCase(Object[] record){
		ExampleTestCaseBean newTestCase = new ExampleTestCaseBean();
		
		newTestCase.setName(record[2].toString());
		newTestCase.setReference(record[3].toString());
		
		_currentProject.addTestCaseBean(newTestCase);
	}
	
	private Long castToLong(Object obj){
		return Long.valueOf(obj.toString());
	}
	
	

}
