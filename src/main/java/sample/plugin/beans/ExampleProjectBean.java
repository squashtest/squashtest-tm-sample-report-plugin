package sample.plugin.beans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ExampleProjectBean {

	private String name;
	
	private List<ExampleTestCaseBean> testCases = new ArrayList<ExampleTestCaseBean>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ExampleTestCaseBean> getTestCases() {
		return testCases;
	}

	public void setTestCases(List<ExampleTestCaseBean> testCases) {
		this.testCases = testCases;
	}
	
	
	public void addTestCaseBean(ExampleTestCaseBean bean){
		testCases.add(bean);
	}
	
	public void addAllTestCaseBean(Collection<ExampleTestCaseBean> beans){
		testCases.addAll(beans);
	}
	
}
