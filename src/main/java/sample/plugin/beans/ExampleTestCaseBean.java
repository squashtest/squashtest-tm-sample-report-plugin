package sample.plugin.beans;

public class ExampleTestCaseBean {

	private String name;
	
	private String reference;

	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}
	
	
}
