-- This is a sample SQL query. Since you cannot know which will be the target database it is well
-- advised to write only simple ANSI queries and avoid advanced formatting functions that might not be available
-- on any platform. On the other hand you will be given some java-time to post process your data.

-- This query uses a parameter. Parameters are ALWAYS named parameters. Named parameters are identified by 
-- a column ':'. Here the argument is ':name'

select proj.project_id as proj_id, proj.name as proj_name, tcln.name as tc_name, tc.reference as tc_ref 
from TEST_CASE tc inner join TEST_CASE_LIBRARY_NODE tcln on tc.tcln_id = tcln.tcln_id  
inner join PROJECT proj on proj.project_id = tcln.project_id 
where tcln.name like :name 
order by proj.project_id asc